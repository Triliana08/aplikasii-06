package imtinan.triliana.appx06

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_pembagian.*
import kotlinx.android.synthetic.main.frag_pembagian.view.*
import kotlinx.android.synthetic.main.frag_perkalian.view.*
import java.text.DecimalFormat

class FragmentPembagian : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var v : View

    var x : Double = 0.0
    var y : Double  = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnKali -> {
                opPembagian()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_pembagian,container,false)
        v.btnKali.setOnClickListener(this)
        return v
    }

    fun opPembagian(){
        x = edX.text.toString().toDouble()
        y = edY.text.toString().toDouble()
        hasil = x / y
        txHasilKali.text =  DecimalFormat("#.##").format(hasil)
    }
}